//
//  DBManager.h
//  LightControl
//
//  Created by Phuchiss Suppakarn on 4/27/2557 BE.
//  Copyright (c) 2557 Phuchiss Suppakarn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject
{
    NSString *databasePath;
}

+(DBManager*)getSharedInstance;
-(BOOL)createDB;
-(BOOL) saveData:(NSString*)registerNumber name:(NSString*)name
             url:(NSString*)url password:(NSString*)password sw:(NSString*)sw type:(NSString *)type icon:(NSString *)icon;
-(NSMutableArray*) findByRegisterNumber:(NSString*)registerNumber;
-(BOOL) deleteData:(NSString*)registerNumber;
-(BOOL) updateData:(NSString*)registerNumber name:(NSString*)name
                url:(NSString*)url password:(NSString*)password sw:(NSString*)sw type:(NSString *)type icon:(NSString *)icon;
-(NSMutableArray*) getAll;
- (NSMutableArray*) getSwtich;
- (NSMutableArray*) getDimmer;
@end
