
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AMWaveTransition
#define COCOAPODS_POD_AVAILABLE_AMWaveTransition
#define COCOAPODS_VERSION_MAJOR_AMWaveTransition 0
#define COCOAPODS_VERSION_MINOR_AMWaveTransition 5
#define COCOAPODS_VERSION_PATCH_AMWaveTransition 0

// DZNEmptyDataSet
#define COCOAPODS_POD_AVAILABLE_DZNEmptyDataSet
#define COCOAPODS_VERSION_MAJOR_DZNEmptyDataSet 1
#define COCOAPODS_VERSION_MINOR_DZNEmptyDataSet 4
#define COCOAPODS_VERSION_PATCH_DZNEmptyDataSet 1

// EFCircularSlider
#define COCOAPODS_POD_AVAILABLE_EFCircularSlider
#define COCOAPODS_VERSION_MAJOR_EFCircularSlider 0
#define COCOAPODS_VERSION_MINOR_EFCircularSlider 1
#define COCOAPODS_VERSION_PATCH_EFCircularSlider 0

// REMenu
#define COCOAPODS_POD_AVAILABLE_REMenu
#define COCOAPODS_VERSION_MAJOR_REMenu 1
#define COCOAPODS_VERSION_MINOR_REMenu 8
#define COCOAPODS_VERSION_PATCH_REMenu 5

// RNGridMenu
#define COCOAPODS_POD_AVAILABLE_RNGridMenu
#define COCOAPODS_VERSION_MAJOR_RNGridMenu 0
#define COCOAPODS_VERSION_MINOR_RNGridMenu 1
#define COCOAPODS_VERSION_PATCH_RNGridMenu 3

