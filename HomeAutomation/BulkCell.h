//
//  BulkCell.h
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/14/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BulkCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *deviceLabel;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;
@property (nonatomic, weak) IBOutlet UILabel *countLabel;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UIButton *switchButton;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;

@property (readwrite) NSString *name;
@property (readwrite) NSString *type;
@property (readwrite) NSString *icon;
@property (readwrite) NSString *regno;
@property (readwrite) NSString *sw;
@property (readwrite) NSString *url;
@property (readwrite) NSString *password;


-(void)readStatus;
- (void)createTimer;

@end
