//
//  ViewController.m
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/6/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import "ViewController.h"
#import "AMWaveTransition.h"
#import "DeviceCell.h"
#import "BulkCell.h"
#import "NavigationViewController.h"
#import "DBManager.h"
#import "FourthViewController.h"
#import "UIScrollView+EmptyDataSet.h"


@interface ViewController () <UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate> {
    NSArray *numbers;
    
#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *data;

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@end

@implementation ViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered target:self.navigationController action:@selector(toggleMenu)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewItem:)];
    
    [self.navigationController.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
	[self.navigationController.navigationBar setBackgroundImage:[self imageWithImage:[UIImage imageNamed:@"navbar"] scaledToSize:CGSizeMake(768, 390)] forBarMetrics:UIBarMetricsDefault];
    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    self.tableView.tableFooterView = [UIView new];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor yellowColor]}];
    
    [self setTitle:@"Home Automation"];
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.data = [[DBManager getSharedInstance]getAll];
//    if(self.data.count == 0)
//    {
//        bool success1 = [[DBManager getSharedInstance]saveData:@"1" name:@"Dining room" url:@"http://localhost/" password:@"1234" sw:@"CH1" type:@"Switch" icon:@"light"];
//        bool success2 = [[DBManager getSharedInstance]saveData:@"1" name:@"Kitchen" url:@"http://localhost/" password:@"1234" sw:@"CH2" type:@"Switch" icon:@"light"];
//        bool success3 = [[DBManager getSharedInstance]saveData:@"1" name:@"Bedroom" url:@"http://localhost/" password:@"1234" sw:@"CH3" type:@"Switch" icon:@"light"];
//    }
    
    if([self.filter isEqual:@"all"])
    {
        self.data = [[DBManager getSharedInstance]getAll];
    } else if([self.filter isEqual:@"switch"])
    {
        self.data = [[DBManager getSharedInstance]getSwtich];
    }else if([self.filter isEqual:@"dimmer"])
    {
        self.data = [[DBManager getSharedInstance]getDimmer];
    }
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.navigationController setDelegate:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* dict = self.data[indexPath.row];
    NSString * name = dict[@"name"];
    NSString * type = dict[@"type"];
    NSString * icon = dict[@"icon"];
    NSString * url = dict[@"url"];
    NSString * password = dict[@"password"];
    NSString * sw = dict[@"sw"];
    NSString * regno = dict[@"regno"];
    if([type isEqual:@"Switch"]){
        
        static NSString *deviceTableIdentifier = @"DeviceCell";
        if ( IDIOM == IPAD ) {
            deviceTableIdentifier = @"DeviceCelliPad";
        }
        DeviceCell *cell = (DeviceCell *)[tableView dequeueReusableCellWithIdentifier:deviceTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DeviceCell" owner:self options:nil];
            
            if ( IDIOM == IPAD ) {
                nib = [[NSBundle mainBundle] loadNibNamed:@"DeviceCelliPad" owner:self options:nil];
            }
            
            cell = [nib objectAtIndex:0];
        }else if([cell isKindOfClass:[BulkCell class]])
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DeviceCell" owner:self options:nil];
            if ( IDIOM == IPAD ) {
                nib = [[NSBundle mainBundle] loadNibNamed:@"DeviceCelliPad" owner:self options:nil];
            }
            
            cell = [nib objectAtIndex:0];
        }
        
        cell.nameLabel.text = name;
        cell.deviceLabel.text = name;
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.thumbnailImageView setImage:[UIImage imageNamed:icon]];
        cell.name = name;
        cell.type = type;
        cell.icon = icon;
        cell.password = password;
        cell.url = url;
        cell.sw = sw;
        cell.regno = regno;
        [cell readStatus];
        [cell createTimer];
        
        return cell;
        
    }else{
        static NSString *deviceTableIdentifier = @"BulkCell";
        if ( IDIOM == IPAD ) {
            deviceTableIdentifier = @"BulkCelliPad";
        }
        BulkCell *cell = (BulkCell *)[tableView dequeueReusableCellWithIdentifier:deviceTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BulkCell" owner:self options:nil];
            
            if ( IDIOM == IPAD ) {
                nib = [[NSBundle mainBundle] loadNibNamed:@"BulkCelliPad" owner:self options:nil];
            }
            
            cell = [nib objectAtIndex:0];
            
        }else if([cell isKindOfClass:[DeviceCell class]])
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BulkCell" owner:self options:nil];
            
            if ( IDIOM == IPAD ) {
                nib = [[NSBundle mainBundle] loadNibNamed:@"BulkCelliPad" owner:self options:nil];
            }
            
            cell = [nib objectAtIndex:0];
        }
        
        cell.nameLabel.text = name;
        cell.deviceLabel.text = name;
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.thumbnailImageView setImage:[UIImage imageNamed:icon]];
        cell.name = name;
        cell.type = type;
        cell.icon = icon;
        cell.password = password;
        cell.url = url;
        cell.sw = sw;
        cell.regno = regno;
        [cell readStatus];
        [cell createTimer];

        return cell;
    }
    
    
}



- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController*)fromVC
                                                 toViewController:(UIViewController*)toVC
{
    if (operation != UINavigationControllerOperationNone) {
        return [AMWaveTransition transitionWithOperation:operation andTransitionType:AMWaveTransitionTypeBounce];
    }
    return nil;
}

- (NSArray*)visibleCells
{
    return [self.tableView visibleCells];
}

- (void)dealloc
{
    [self.navigationController setDelegate:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if( IS_IPHONE_5 ) {
        [self performSegueWithIdentifier:@"SelectedRow" sender:cell];
    }else{
        [self performSegueWithIdentifier:@"SelectedRow2" sender:cell];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"SelectedRow"]) {
        DeviceCell *cell = (DeviceCell *)sender;
        FourthViewController *myVC = [segue destinationViewController];
        myVC.regno = cell.regno;
        myVC.name = cell.name;
        myVC.url = cell.url;
        myVC.password = cell.password;
        myVC.sw = cell.sw;
        myVC.type = cell.type;
        myVC.icon = cell.icon;
    }else{
        DeviceCell *cell = (DeviceCell *)sender;
        FourthViewController *myVC = [segue destinationViewController];
        myVC.regno = cell.regno;
        myVC.name = cell.name;
        myVC.url = cell.url;
        myVC.password = cell.password;
        myVC.sw = cell.sw;
        myVC.type = cell.type;
        myVC.icon = cell.icon;
    }
}

- (void)addNewItem:(id)sender
{
    [self showListChanel];
}

#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    NSLog(@"Dismissed with item %ld: %@", (long)itemIndex, item.title);
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    
    if ( IDIOM == IPAD ) {
        mainStoryboard = [UIStoryboard storyboardWithName:@"iPad"
                                                   bundle: nil];
    }
    
    FourthViewController *controller = (FourthViewController*)[mainStoryboard
                                                   instantiateViewControllerWithIdentifier: @"FourthViewController"];
    
    if( !IS_IPHONE_5 && IDIOM != IPAD ) {
        controller = (FourthViewController*)[mainStoryboard
                                              instantiateViewControllerWithIdentifier: @"Fourth2ViewController"];
    }
    
    controller.type = item.title;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)showListChanel {
    NSInteger numberOfOptions = 2;
    NSArray *options = @[
                         @"Switch",
                         @"Dimmer"
                         ];
    RNGridMenu *av = [[RNGridMenu alloc] initWithTitles:[options subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    //    av.itemTextAlignment = NSTextAlignmentLeft;
    av.itemFont = [UIFont boldSystemFontOfSize:18];
    av.itemSize = CGSizeMake(150, 55);
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 78;
//}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Devices";
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:22.0];
    UIColor *textColor = [UIColor colorWithRed:0.61166 green:0.62267 blue:0.686862 alpha:1.0];
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Tap Add and add things to Owns";
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    UIColor *textColor = [UIColor colorWithRed:0.61166 green:0.62267 blue:0.686862 alpha:1.0];
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    if (paragraph) [attributes setObject:paragraph forKey:NSParagraphStyleAttributeName];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
    return attributedString;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"placeholder"];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 10.0;
}

@end
