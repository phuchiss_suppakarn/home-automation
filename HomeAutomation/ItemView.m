//
//  ItemView.m
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/13/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import "ItemView.h"

@implementation ItemView

@synthesize imageView = _imageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
