//
//  DeviceCell.h
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/13/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *deviceLabel;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;
@property (nonatomic, weak) IBOutlet UIButton *switchButton;
@property (readwrite) NSString *name;
@property (readwrite) NSString *type;
@property (readwrite) NSString *icon;
@property (readwrite) NSString *regno;
@property (readwrite) NSString *sw;
@property (readwrite) NSString *url;
@property (readwrite) NSString *password;

-(void)readStatus;
- (void)createTimer;
- (void)deleteTimer;


@end
