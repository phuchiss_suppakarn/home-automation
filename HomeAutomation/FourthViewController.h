//
//  FourthViewController.h
//  Demo
//
//  Created by Andrea Mazzini on 01/05/14.
//  Copyright (c) 2014 Fancy Pixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeView.h"
#import "RNGridMenu.h"

@interface FourthViewController : UIViewController <SwipeViewDelegate, SwipeViewDataSource, UITextFieldDelegate, RNGridMenuDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *iconPicker;

@property (weak, nonatomic) IBOutlet SwipeView *iconSwipe;

@property (weak, nonatomic) IBOutlet UITextField *urlText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *swText;
@property (nonatomic, strong) NSString *regno;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *sw;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *type;
@property (weak, nonatomic) IBOutlet UIButton *chButton;
@property (weak, nonatomic) IBOutlet UIButton *iconButton;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

-(void)udpateName:(NSString *)name;

@end
