//
//  BulkCell.m
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/14/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import "BulkCell.h"
#import "EFCircularSlider.h"
#import "HTMLNode.h"
#import "HTMLParser.h"

@interface BulkCell () {
    NSArray *numbers;
     NSTimer *timer;
}
    
@property (nonatomic) BOOL toggleIsOn;
@property (nonatomic) int count;

@end

@implementation BulkCell {
    EFCircularSlider* hourSlider;
}

@synthesize nameLabel = _nameLabel;
@synthesize statusLabel = _statusLabel;
@synthesize deviceLabel = _deviceLabel;
@synthesize thumbnailImageView = _thumbnailImageView;
@synthesize countLabel = _countLabel;
@synthesize switchButton = _switchButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}



- (void)awakeFromNib
{
    self.count = 0;
    
    numbers = @[@(1), @(2), @(3), @(4), @(5), @(6)];
    
    NSInteger numberOfSteps = ((float)[numbers count] - 1);
    self.slider.maximumValue = numberOfSteps;
    self.slider.minimumValue = 0;
    self.slider.value = 0;
    
    self.slider.continuous = YES;
    [self.slider addTarget:self
               action:@selector(valueChanged:)
     forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)valueChanged:(UISlider *)sender {
    [self stopTimer];
    // round the slider position to the nearest index of the numbers array
    NSUInteger index = (NSUInteger)(_slider.value + 0.5);
    [_slider setValue:index animated:NO];
    NSNumber *number = numbers[index]; // <-- This numeric value you want
    NSLog(@"sliderIndex: %i", (int)index);
    NSLog(@"number: %@", number);
    NSLog(@"sliderValue: %f", self.slider.value);
    if([number intValue] == 1) {
        self.levelLabel.textColor = [UIColor grayColor];
    }else{
        self.levelLabel.textColor = [UIColor greenColor];
    }
    [self.levelLabel setText:[NSString stringWithFormat:@"%d",[number intValue] - 1]];
    
    NSString *selectedLebel = @"A";
    if((int)self.slider.value == 0)
        selectedLebel = @"A";
    else if((int)self.slider.value == 1)
        selectedLebel = @"B";
    else if((int)self.slider.value == 2)
        selectedLebel = @"C";
    else if((int)self.slider.value == 3)
        selectedLebel = @"D";
    else if((int)self.slider.value == 4)
        selectedLebel = @"E";
    else if((int)self.slider.value == 5)
        selectedLebel = @"F";
    
    NSString *request = [NSString stringWithFormat:@"%@/%@/%@", self.url,self.password,selectedLebel];
    
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:request]];
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if (error) {
            NSLog(@"Error: %@", error);
            [self setValueFromHTMLString:result];
            [self createTimer];
            return;
        }
        
        [self setValueFromHTMLString:result];
        [self createTimer];
    }];

}

-(void)hourDidChange:(EFCircularSlider*)slider {
    int newVal = (int)slider.currentValue ? (int)slider.currentValue : 4;
}

- (IBAction)toggle:(id)sender
{
//    if(_toggleIsOn){
//        //do anything else you want to do.
//        _statusLabel.text = @"OFF";
//    }
//    else {
//        //do anything you want to do.
//        _statusLabel.text = @"ON";
//    }
//    _toggleIsOn = !_toggleIsOn;
    
    self.count = self.count + 1;
    if(self.count > 4)
        self.count = 0;
    
    if(self.count == 0){
        _statusLabel.text = @"OFF";
        _toggleIsOn = NO;
        self.countLabel.text = @"";
        self.countLabel.textColor = [UIColor colorWithRed:0.483657 green:0.483642 blue:0.48365 alpha:1.0];
    }else{
        _statusLabel.text = @"ON";
        _toggleIsOn = YES;
        self.countLabel.text = [NSString stringWithFormat:@"%d",self.count];
        self.countLabel.textColor = [UIColor colorWithRed:0.370182 green:0.690336 blue:0.333354 alpha:1.0];
    }
    [self.switchButton setImage:[UIImage imageNamed:_toggleIsOn ? @"circleon" :@"circle"] forState:UIControlStateNormal];
//    self.switchButton.imageView.image = [self changeColorForImage:self.switchButton.imageView.image toColor:[UIColor colorWithRed:0.370182 green:0.690336 blue:0.333354 alpha:1.0]];
    
    
    
}

- (UIImage *) changeColorForImage:(UIImage *)image toColor:(UIColor*)color {
    UIGraphicsBeginImageContext(image.size);
    
    CGRect contextRect;
    contextRect.origin.x = 0.0f;
    contextRect.origin.y = 0.0f;
    contextRect.size = [image size];
    // Retrieve source image and begin image context
    CGSize itemImageSize = [image size];
    CGPoint itemImagePosition;
    itemImagePosition.x = ceilf((contextRect.size.width - itemImageSize.width) / 2);
    itemImagePosition.y = ceilf((contextRect.size.height - itemImageSize.height) );
    
//    UIGraphicsBeginImageContext(contextRect.size);
    UIGraphicsBeginImageContextWithOptions(contextRect.size, NO, 0.0);
    CGContextRef c = UIGraphicsGetCurrentContext();
    // Setup shadow
    // Setup transparency layer and clip to mask
    CGContextBeginTransparencyLayer(c, NULL);
    CGContextScaleCTM(c, 1.0, -1.0);
    CGContextClipToMask(c, CGRectMake(itemImagePosition.x, -itemImagePosition.y, itemImageSize.width, -itemImageSize.height), [image CGImage]);
    // Fill and end the transparency layer
    
    
    const float* colors = CGColorGetComponents( color.CGColor );
    CGContextSetRGBFillColor(c, colors[0], colors[1], colors[2], .75);
    
    contextRect.size.height = -contextRect.size.height;
    contextRect.size.height -= 15;
    CGContextFillRect(c, contextRect);
    CGContextEndTransparencyLayer(c);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (void)stopTimer {
    if ([timer isValid]) {
        [timer invalidate];
    }
    timer = nil;
}

- (void)createTimer {
    if(timer == nil) {
        timer =  [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(timerTicked:) userInfo:nil repeats:YES];
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    if(!newSuperview) {
        NSLog(@"timer invalidated");
        if ([timer isValid]) {
            [timer invalidate];
        }
    }
}

- (void)timerTicked:(NSTimer*)timer {
    [self readStatus];
}

-(void)readStatus {
    [self stopTimer];
    NSString *request = [NSString stringWithFormat:@"%@/%@", self.url,self.password];
    
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:request]];
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if (error) {
            NSLog(@"Error: %@", error);
            [self setValueFromHTMLString:result];
            [self createTimer];
            return;
        }
        
        [self setValueFromHTMLString:result];
        [self createTimer];
    }];
}

-(void)setValueFromHTMLString:(NSString *)html{
    NSError *error;
    HTMLParser *parser = [[HTMLParser alloc] initWithString:html error:&error];
    
    if (error) {
        NSLog(@"Error: %@", error);
    }
    
    HTMLNode *bodyNode = [parser body];
    NSString *levelString = [NSString stringWithFormat:@"%d",(int)self.slider.value + 1];
    NSString *selectedLebel = @"A";
    if((int)self.slider.value == 0)
        selectedLebel = @"A";
    else if((int)self.slider.value == 1)
        selectedLebel = @"B";
    else if((int)self.slider.value == 2)
        selectedLebel = @"C";
    else if((int)self.slider.value == 3)
        selectedLebel = @"D";
    else if((int)self.slider.value == 4)
        selectedLebel = @"E";
    else if((int)self.slider.value == 5)
        selectedLebel = @"F";
    
    NSArray *inputNodes1 = [bodyNode findChildrenWithAttribute:@"id" matchingName:@"status" allowPartial:true];
    
    
    if([inputNodes1 count] > 0){
        NSString *status = [inputNodes1[0] contents];
        int lebelInt = [status intValue];
        self.slider.value = lebelInt - 1;
        self.levelLabel.text = [NSString stringWithFormat:@"%d",lebelInt - 1];
//        UIImage *image = [UIImage imageNamed:self.icon];
//        [self.thumbnailImageView setImage:[self changeColorForImage:image toColor:[UIColor colorWithRed:0.350009 green:0.907159 blue:0.223784 alpha:1.0]]];
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-on",self.icon]];
        [self.thumbnailImageView setImage:image];
        
    }else{
        self.slider.value = 0;
        self.levelLabel.text = [NSString stringWithFormat:@"%d",0];
        UIImage *image = [UIImage imageNamed:self.icon];
        [self.thumbnailImageView setImage:image];
    }
    
    if(self.slider.value == 0) {
        self.levelLabel.textColor = [UIColor grayColor];
    }else{
        self.levelLabel.textColor = [UIColor greenColor];
    }
    
}

@end
