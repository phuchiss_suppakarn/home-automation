//
//  ItemView.h
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/13/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
