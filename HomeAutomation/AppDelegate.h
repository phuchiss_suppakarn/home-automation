//
//  AppDelegate.h
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/6/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
