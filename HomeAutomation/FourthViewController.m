//
//  FourthViewController.m
//  Demo
//
//  Created by Andrea Mazzini on 01/05/14.
//  Copyright (c) 2014 Fancy Pixel. All rights reserved.
//

#import "FourthViewController.h"
#import "ItemView.h"
#import "DBManager.h"
#import "DeviceCell.h"

@interface FourthViewController ()
{
    NSArray *_pickerData;
    NSString *selectedIcon;
}

@end

@implementation FourthViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    _pickerData = @[@"light", @"tv", @"home", @"computer", @"newspaper", @"kitchen"];
    
    self.iconPicker.dataSource = self;
    self.iconPicker.delegate = self;
    
    _iconSwipe.alignment = SwipeViewAlignmentCenter;
    _iconSwipe.pagingEnabled = YES;
    _iconSwipe.itemsPerPage = 4;
    _iconSwipe.truncateFinalPage = YES;
    
    
    
    if([_type isEqualToString:@"Switch"]){
        if(_sw == nil)
            _sw = @"CH1";
        if(_icon == nil)
            _icon = @"light";
    }else {
        _sw = @"";
        if(_icon == nil)
            _icon = @"dimmer+";
        [self.chButton setHidden:true];
    }
    
    self.nameText.text = _name;
    self.urlText.text = _url;
    self.passwordText.text = _password;
    self.swText.text = _sw;
    self.nameText.delegate = self;
    self.urlText.delegate = self;
    self.passwordText.delegate =self;
    selectedIcon = _icon;
    //self.chButton.titleLabel.text = _sw;
    [self.chButton setTitle:_sw forState:UIControlStateNormal];
//    [self.iconButton.imageView setImage:[UIImage imageNamed:_icon]];
    [self.iconButton setImage:[UIImage imageNamed:_icon] forState:UIControlStateNormal];
    self.title = self.name;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)udpateName:(NSString *)name
{
    self.nameText.text = name;
}


// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return (int)_pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    return _pickerData.count;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (!view)
    {
    	view = [[NSBundle mainBundle] loadNibNamed:@"ItemView" owner:self options:nil][0];
    }
    
    ItemView *nv = (ItemView *)[[NSBundle mainBundle] loadNibNamed:@"ItemView" owner:self options:nil][0];
    [nv.imageView setImage:[UIImage imageNamed:_pickerData[index]]];
    
    if(index == 0) {
        nv.imageView.image = [self changeColorForImage:nv.imageView.image toColor:[UIColor colorWithRed:0.370182 green:0.690336 blue:0.333354 alpha:1.0]];
    }
    
    return nv;
}

- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index
{
    for (int i=0; i<_pickerData.count; i++)
    {
        ItemView *view = (ItemView *)[swipeView itemViewAtIndex:i];
        [view.imageView setImage:[UIImage imageNamed:_pickerData[i]]];
    }
    ItemView *view = (ItemView *)[swipeView itemViewAtIndex:index];
    view.imageView.image = [self changeColorForImage:view.imageView.image toColor:[UIColor colorWithRed:0.370182 green:0.690336 blue:0.333354 alpha:1.0]];
}

- (UIImage *) changeColorForImage:(UIImage *)image toColor:(UIColor*)color {
    UIGraphicsBeginImageContext(image.size);
    
    CGRect contextRect;
    contextRect.origin.x = 0.0f;
    contextRect.origin.y = 0.0f;
    contextRect.size = [image size];
    // Retrieve source image and begin image context
    CGSize itemImageSize = [image size];
    CGPoint itemImagePosition;
    itemImagePosition.x = ceilf((contextRect.size.width - itemImageSize.width) / 2);
    itemImagePosition.y = ceilf((contextRect.size.height - itemImageSize.height) );
    
    UIGraphicsBeginImageContext(contextRect.size);
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    // Setup shadow
    // Setup transparency layer and clip to mask
    CGContextBeginTransparencyLayer(c, NULL);
    CGContextScaleCTM(c, 1.0, -1.0);
    CGContextClipToMask(c, CGRectMake(itemImagePosition.x, -itemImagePosition.y, itemImageSize.width, -itemImageSize.height), [image CGImage]);
    // Fill and end the transparency layer
    
    
    const float* colors = CGColorGetComponents( color.CGColor );
    CGContextSetRGBFillColor(c, colors[0], colors[1], colors[2], .75);
    
    contextRect.size.height = -contextRect.size.height;
    contextRect.size.height -= 15;
    CGContextFillRect(c, contextRect);
    CGContextEndTransparencyLayer(c);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

- (IBAction)saveData:(id)sender {
    bool success = NO;
    if(_regno == nil){
        success =  [[DBManager getSharedInstance]saveData:@"1" name:self.nameText.text url:self.urlText.text password:self.passwordText.text sw:self.chButton.titleLabel.text type:_type icon:selectedIcon];
    }else{
        success = [[DBManager getSharedInstance]updateData:_regno name:self.nameText.text url:self.urlText.text password:self.passwordText.text sw:self.chButton.titleLabel.text type:_type icon:selectedIcon ];
    }
    
    if(success)
        [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)deleteData:(id)sender {
    bool success = NO;
    if(_regno != nil){
        success =  [[DBManager getSharedInstance] deleteData:_regno];
    }else{
        success = YES;
    }
    
    if(success)
        [self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)selectChanel:(id)sender {
    [self showListChanel];
}

- (IBAction)selectIcon:(id)sender {
    [self showListIcon];
}

#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    NSLog(@"Dismissed with item %ld: %@", (long)itemIndex, item.title);
    
    if(item.title != nil){
        self.chButton.titleLabel.text = item.title;
    }else{
        selectedIcon = _pickerData[itemIndex];
        [self.iconButton.imageView setImage:[UIImage imageNamed:selectedIcon]];
    }
}

- (void)showListIcon {
    
    NSInteger numberOfOptions = 4;
    
    NSArray *images = @[];
    
    if([_type isEqualToString:@"Switch"]){
        numberOfOptions = 4;
        images = @[
                   [UIImage imageNamed:@"light"],
                   [UIImage imageNamed:@"tv"],
                   [UIImage imageNamed:@"home"],
                   [UIImage imageNamed:@"computer"]
                   ];
    }else {
        numberOfOptions = 1;
        images = @[
                   [UIImage imageNamed:@"dimmer+"]
                   ];
    }

    
    
    RNGridMenu *av = [[RNGridMenu alloc] initWithImages:[images subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

- (void)showListChanel {
    NSInteger numberOfOptions = 3;
    NSArray *options = @[
                         @"CH1",
                         @"CH2",
                         @"CH3"
                         ];
    RNGridMenu *av = [[RNGridMenu alloc] initWithTitles:[options subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    //    av.itemTextAlignment = NSTextAlignmentLeft;
    av.itemFont = [UIFont boldSystemFontOfSize:18];
    av.itemSize = CGSizeMake(150, 55);
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

@end
