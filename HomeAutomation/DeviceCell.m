//
//  DeviceCell.m
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/13/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import "DeviceCell.h"
#import "HTMLParser.h"
#import "HTMLNode.h"

@interface DeviceCell () {
    NSTimer *timer;
    BOOL isClick;
}

@property (nonatomic) BOOL toggleIsOn;


@end

@implementation DeviceCell


@synthesize nameLabel = _nameLabel;
@synthesize deviceLabel = _deviceLabel;
@synthesize statusLabel = _statusLabel;
@synthesize thumbnailImageView = _thumbnailImageView;
@synthesize switchButton = _switchButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)toggle:(id)sender
{
    [self stopTimer];
    isClick = YES;
    if(_toggleIsOn){
        //do anything else you want to do.
        _statusLabel.text = @"OFF";
        
        NSString *request = [NSString stringWithFormat:@"%@/%@/%@", self.url,self.password,[self.sw uppercaseString]];
        
        NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:request] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if (error) {
                NSLog(@"Error: %@", error);
                [self setValueFromHTMLString:result];
                [self createTimer];
                isClick = NO;
                return;
            }
            
            [self setValueFromHTMLString:result];
            [self createTimer];
            isClick = NO;
        }];
    }
    else {
        //do anything you want to do.
        _statusLabel.text = @"ON";
        
        NSString *request = [NSString stringWithFormat:@"%@/%@/%@", self.url,self.password,[self.sw uppercaseString]];
        
        NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:request] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];

        
        [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if (error) {
                NSLog(@"Error: %@", error);
                [self setValueFromHTMLString:result];
                [self createTimer];
                isClick = NO;
                return;
            }
            
            [self setValueFromHTMLString:result];
            [self createTimer];
            isClick = NO;
        }];
    }
    _toggleIsOn = !_toggleIsOn;
    [self.switchButton setImage:[UIImage imageNamed:_toggleIsOn ? @"switchon" :@"switchoff"] forState:UIControlStateNormal];
}

- (void)stopTimer {
    if ([timer isValid]) {
        [timer invalidate];
    }
    timer = nil;
}

- (void)createTimer {
    if(timer == nil) {
        timer =  [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(timerTicked:) userInfo:nil repeats:YES];
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    if(!newSuperview) {
        NSLog(@"timer invalidated");
        if ([timer isValid]) {
            [timer invalidate];
        }
    }
}

- (void)timerTicked:(NSTimer*)timer {
    [self readStatus];
}

-(void)readStatus {
    [self stopTimer];
    NSString *request = [NSString stringWithFormat:@"%@/%@", self.url,self.password];

    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:request] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if (error) {
            NSLog(@"Error: %@", error);
            if(!isClick) {
                [self setValueFromHTMLString:result];
            }
            [self createTimer];
            return;
        }
        
        if(!isClick) {
            [self setValueFromHTMLString:result];
        }
        [self createTimer];
    }];
}

-(void)setValueFromHTMLString:(NSString *)html{
    NSError *error;
    HTMLParser *parser = [[HTMLParser alloc] initWithString:html error:&error];
    
    if (error) {
        NSLog(@"Error: %@", error);
    }
    
    HTMLNode *bodyNode = [parser body];
    
    NSArray *inputNodes1 = [bodyNode findChildrenWithAttribute:@"id" matchingName:[self.sw lowercaseString] allowPartial:true];
    
    if([inputNodes1 count] > 0){
        NSString *status = [inputNodes1[0] contents];
        if([status  isEqualToString:@"ON"]){
            _statusLabel.text = @"ON";
            _toggleIsOn = YES;
        }else{
            _statusLabel.text = @"OFF";
            _toggleIsOn = NO;
        }
//        UIImage *image = [UIImage imageNamed:self.icon];
//        [self.thumbnailImageView setImage:[self changeColorForImage:image toColor:[UIColor greenColor]]];
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-on",self.icon]];
        [self.thumbnailImageView setImage:image];
    }else{
        _statusLabel.text = @"OFF";
        _toggleIsOn = NO;
        UIImage *image = [UIImage imageNamed:self.icon];
        [self.thumbnailImageView setImage:image];
    }
    
    [self.switchButton setImage:[UIImage imageNamed:_toggleIsOn ? @"switchon" :@"switchoff"] forState:UIControlStateNormal];

}

- (UIImage *) changeColorForImage:(UIImage *)image toColor:(UIColor*)color {
    UIGraphicsBeginImageContext(image.size);
    
    CGRect contextRect;
    contextRect.origin.x = 0.0f;
    contextRect.origin.y = 0.0f;
    contextRect.size = [image size];
    // Retrieve source image and begin image context
    CGSize itemImageSize = [image size];
    CGPoint itemImagePosition;
    itemImagePosition.x = ceilf((contextRect.size.width - itemImageSize.width) / 2);
    itemImagePosition.y = ceilf((contextRect.size.height - itemImageSize.height) );
    
    UIGraphicsBeginImageContext(contextRect.size);
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    // Setup shadow
    // Setup transparency layer and clip to mask
    CGContextBeginTransparencyLayer(c, NULL);
    CGContextScaleCTM(c, 1.0, -1.0);
    CGContextClipToMask(c, CGRectMake(itemImagePosition.x, -itemImagePosition.y, itemImageSize.width, -itemImageSize.height), [image CGImage]);
    // Fill and end the transparency layer
    
    
    const float* colors = CGColorGetComponents( color.CGColor );
    CGContextSetRGBFillColor(c, colors[0], colors[1], colors[2], .75);
    
    contextRect.size.height = -contextRect.size.height;
    contextRect.size.height -= 15;
    CGContextFillRect(c, contextRect);
    CGContextEndTransparencyLayer(c);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

@end
