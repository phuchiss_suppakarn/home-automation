//
//  main.m
//  HomeAutomation
//
//  Created by Phuchiss Suppakarn on 7/6/2557 BE.
//  Copyright (c) 2557 phuchiss. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
