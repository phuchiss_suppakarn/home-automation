//
//  DBManager.m
//  LightControl
//
//  Created by Phuchiss Suppakarn on 4/27/2557 BE.
//  Copyright (c) 2557 Phuchiss Suppakarn. All rights reserved.
//

#import "DBManager.h"
static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation DBManager

+(DBManager*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"lightcontrol2.db"]];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt =
            "create table if not exists settings (regno integer primary key, name text, url text, password text, sw text, type text, icon text)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg)
                != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

- (BOOL) saveData:(NSString*)registerNumber name:(NSString*)name
              url:(NSString*)url password:(NSString*)password sw:(NSString*)sw type:(NSString *)type icon:(NSString *)icon;
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into settings (name, url, password, sw, type, icon) values (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\", \"%@\")",
                               name, url, password, sw, type, icon];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        sqlite3_reset(statement);
    }
    return NO;
}

- (BOOL) updateData:(NSString*)registerNumber name:(NSString*)name
              url:(NSString*)url password:(NSString*)password sw:(NSString*)sw type:(NSString *)type icon:(NSString *)icon;
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update settings set name=\"%@\", url=\"%@\", password=\"%@\", sw=\"%@\", type=\"%@\", icon=\"%@\" where regno=\"%@\"",
                               name, url, password, sw, type, icon, registerNumber];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(database, update_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        sqlite3_reset(statement);
    }
    return NO;
}

-(BOOL) deleteData:(NSString*)registerNumber{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"delete from settings where regno=\"%@\"",registerNumber];
        const char *delete_stmt = [querySQL UTF8String];
        sqlite3_prepare_v2(database, delete_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            return YES;
        }
        else {
            return NO;
        }
        sqlite3_reset(statement);
    }
    return NO;
}

- (NSMutableArray*) findByRegisterNumber:(NSString*)registerNumber
{
    NSMutableArray *arrResult = [[NSMutableArray alloc]init];
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"select name, url, password, sw, type, icon, regno from settings where regno=\"%@\"",registerNumber];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char *name;
                char *url;
                char *password;
                char *sw;
                char *type;
                char *icon;
                char *regno;
                
                if(sqlite3_column_text(statement, 0) != NULL) {
                    
                    name = sqlite3_column_text(statement, 0);
                    url = sqlite3_column_text(statement, 1);
                    password = sqlite3_column_text(statement, 2);
                    sw = sqlite3_column_text(statement, 3);
                    type = sqlite3_column_text(statement, 4);
                    icon = sqlite3_column_text(statement, 5);
                    regno = sqlite3_column_text(statement, 6);
                    
                    NSString *strName = [NSString stringWithUTF8String:name];
                    NSString *strUrl = [NSString stringWithUTF8String:url];
                    NSString *strPassword = [NSString stringWithUTF8String:password];
                    NSString *strSw = [NSString stringWithUTF8String:sw];
                    NSString *strType = [NSString stringWithUTF8String:type];
                    NSString *strIcon = [NSString stringWithUTF8String:icon];
                    NSString *strRegno = [NSString stringWithUTF8String:regno];
                    
                    NSMutableDictionary *dictResult = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strName, @"name", strUrl, @"url", strPassword, @"password", strSw, @"sw", strType, @"type", strIcon, @"icon", strRegno, @"regno", nil];
                    [arrResult addObject:dictResult];
                }
            }
            
            sqlite3_reset(statement);
            return arrResult;                }
    }
    return nil;
}

- (NSMutableArray*) getAll
{
    NSMutableArray *arrResult = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"select name, url, password, sw, type, icon, regno from settings"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char *name;
                char *url;
                char *password;
                char *sw;
                char *type;
                char *icon;
                char *regno;
                
                if(sqlite3_column_text(statement, 0) != NULL) {
                    
                    name = sqlite3_column_text(statement, 0);
                    url = sqlite3_column_text(statement, 1);
                    password = sqlite3_column_text(statement, 2);
                    sw = sqlite3_column_text(statement, 3);
                    type = sqlite3_column_text(statement, 4);
                    icon = sqlite3_column_text(statement, 5);
                    regno = sqlite3_column_text(statement, 6);
                    
                    NSString *strName = [NSString stringWithUTF8String:name];
                    NSString *strUrl = [NSString stringWithUTF8String:url];
                    NSString *strPassword = [NSString stringWithUTF8String:password];
                    NSString *strSw = [NSString stringWithUTF8String:sw];
                    NSString *strType = [NSString stringWithUTF8String:type];
                    NSString *strIcon = [NSString stringWithUTF8String:icon];
                    NSString *strRegno = [NSString stringWithUTF8String:regno];
                    
                    NSMutableDictionary *dictResult = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strName, @"name", strUrl, @"url", strPassword, @"password", strSw, @"sw", strType, @"type", strIcon, @"icon", strRegno, @"regno", nil];
                    [arrResult addObject:dictResult];
                }
            }
            
            sqlite3_reset(statement);
            return arrResult;
        }
    }
    return nil;
}

- (NSMutableArray*) getSwtich
{
    NSMutableArray *arrResult = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"select name, url, password, sw, type, icon, regno from settings where type=\"Switch\""];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char *name;
                char *url;
                char *password;
                char *sw;
                char *type;
                char *icon;
                char *regno;
                
                if(sqlite3_column_text(statement, 0) != NULL) {
                    
                    name = sqlite3_column_text(statement, 0);
                    url = sqlite3_column_text(statement, 1);
                    password = sqlite3_column_text(statement, 2);
                    sw = sqlite3_column_text(statement, 3);
                    type = sqlite3_column_text(statement, 4);
                    icon = sqlite3_column_text(statement, 5);
                    regno = sqlite3_column_text(statement, 6);
                    
                    NSString *strName = [NSString stringWithUTF8String:name];
                    NSString *strUrl = [NSString stringWithUTF8String:url];
                    NSString *strPassword = [NSString stringWithUTF8String:password];
                    NSString *strSw = [NSString stringWithUTF8String:sw];
                    NSString *strType = [NSString stringWithUTF8String:type];
                    NSString *strIcon = [NSString stringWithUTF8String:icon];
                    NSString *strRegno = [NSString stringWithUTF8String:regno];
                    
                    NSMutableDictionary *dictResult = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strName, @"name", strUrl, @"url", strPassword, @"password", strSw, @"sw", strType, @"type", strIcon, @"icon", strRegno, @"regno", nil];
                    [arrResult addObject:dictResult];
                }
            }
            
            sqlite3_reset(statement);
            return arrResult;
        }
    }
    return nil;
}

- (NSMutableArray*) getDimmer
{
    NSMutableArray *arrResult = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:
                              @"select name, url, password, sw, type, icon, regno from settings where type=\"Dimmer\""];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc]init];
        if (sqlite3_prepare_v2(database,
                               query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char *name;
                char *url;
                char *password;
                char *sw;
                char *type;
                char *icon;
                char *regno;
                
                if(sqlite3_column_text(statement, 0) != NULL) {
                    
                    name = sqlite3_column_text(statement, 0);
                    url = sqlite3_column_text(statement, 1);
                    password = sqlite3_column_text(statement, 2);
                    sw = sqlite3_column_text(statement, 3);
                    type = sqlite3_column_text(statement, 4);
                    icon = sqlite3_column_text(statement, 5);
                    regno = sqlite3_column_text(statement, 6);
                    
                    NSString *strName = [NSString stringWithUTF8String:name];
                    NSString *strUrl = [NSString stringWithUTF8String:url];
                    NSString *strPassword = [NSString stringWithUTF8String:password];
                    NSString *strSw = [NSString stringWithUTF8String:sw];
                    NSString *strType = [NSString stringWithUTF8String:type];
                    NSString *strIcon = [NSString stringWithUTF8String:icon];
                    NSString *strRegno = [NSString stringWithUTF8String:regno];
                    
                    NSMutableDictionary *dictResult = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strName, @"name", strUrl, @"url", strPassword, @"password", strSw, @"sw", strType, @"type", strIcon, @"icon", strRegno, @"regno", nil];
                    [arrResult addObject:dictResult];
                }
            }
            
            sqlite3_reset(statement);
            return arrResult;
        }
    }
    return nil;
}
@end